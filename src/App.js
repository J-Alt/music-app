import "./App.css";
import {useState} from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/Header";
import Browse from "./components/Browse";
import ArtistPage from "./components/ArtistPage";
import artists from "./components/assets/db";
import NotFound from "./components/NotFound";
import ScrollToTop from "./components/ScrollToTop";

const App = () => {
const [allArtists] = useState(artists)

  return(
    <Router>
    <ScrollToTop/>
      <div className="App">
        <Header />
        <Switch>
          <Route exact path="/">
            <Browse artists={allArtists} />
          </Route>
          <Route path="/artist/:id" component={ArtistPage} />
          <Route path="" component={NotFound} />
        </Switch>
      </div>
    </Router>
  )
}

export default App;
