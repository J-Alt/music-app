import React, { useState } from "react";
import Albums from "./Albums";
import artists from "./assets/db";

const ArtistPage = (props) => {
  const [artist] = useState(
    artists.find((el) => el.id == props.match.params.id)
  );

  return (
    <div className="ArtistPage">
      <img src={`../imgs/${artist.cover}.jpg`} />
      <h2>{artist.name}</h2>
      <p>{artist.bio}</p>
      <Albums albums={artist.albums} />
    </div>
  );
};

export default ArtistPage;
