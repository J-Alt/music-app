const Albums = (props) => (
    <div className="Albums">
{props.albums.map((el, i) => <img src={require(`./assets/albums/${el.cover}.jpg`).default} key={i} alt="album"/>)}
    </div>
);

export default Albums;