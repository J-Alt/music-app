import {Link} from 'react-router-dom';
const Header = () => (
<Link to="/" className="header-link">
<div className="Header">
<h1>MUSIC-DB</h1>
</div>
</Link>
);

export default Header;