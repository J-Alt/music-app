import Artist from "./BrowseArtist";

const Browse = (props) => (
    <div className="Browse">
        <h2>Browse the artists</h2>
        {props.artists.map((el) => (
          <Artist img={el.cover} name={el.name} key={el.id} id={el.id}/>
        ))}
      </div>
);

export default Browse;
