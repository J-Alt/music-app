import {Link} from 'react-router-dom';

const Artist = (props) => {
    return(
       <Link to={`/artist/${props.id}`}>
        <div className="Artist">
      <div className="artist-img" style={{backgroundImage:`url(./imgs/${props.img}.jpg)` }}></div>
    <span>{props.name}</span>
  </div>
       </Link>
    )
};

export default Artist;
