import { useEffect, useState } from "react";

const NotFound = (props) => {
  const [counter, setCounter] = useState(4);

  let interval;

  useEffect(()=>{
    interval = setInterval(() => {
      if (counter > 0) {
        setCounter(counter => counter - 1)
      } else {
       props.history.push("/");
      }
    }, 1000);


    return () => {
      clearInterval(interval);
  }
  })
 
  return (
    <div className="NotFound">
      <h2>
        This page does not exist. You will be redirected in{" "}
        {<span>{counter}</span>}
      </h2>
    </div>
  );
};

export default NotFound;